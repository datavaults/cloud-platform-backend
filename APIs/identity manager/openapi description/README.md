**Basic API calls for DataVaults**


| **Method** | **Generic API** | **Datavaults API** | **Short Description** |
| --- | --- | --- | --- |
| **POST** | {realm}/protocol/openid-connect/token | https://datavaults-auth.euprojects.net/auth/personal-app/protocol/openid-connect/token | Token retrieval for user or service |
| **PUT, DELETE** | /{realm}/users/{id} | https://datavaults-auth.euprojects.net/auth/personal-app/users/{id} | User Deletion, Update |
| **POST** | /{realm}/users | https://datavaults-auth.euprojects.net/auth/personal-app/users | Create User and set user attributes |
| **GET** | {realm}/users?username={username} | https://datavaults-auth.euprojects.net/auth/personal-app/users?username={username} | Retrieve user attributes by username |
| **GET** | {realm}/users/{id} | https://datavaults-auth.euprojects.net/auth/personal-app/{id} | Retrieve user attributes by user-id |
| **GET** | /{realm}/users | https://datavaults-auth.euprojects.net/auth/personal-app/users | Retrieve all realm users |
| **PUT** | /{realm}/users/{id}/reset-password | https://datavaults-auth.euprojects.net/auth/personal-app/users/{id}/reset-password | Reset the user password |
| **GET** | ​/{realm}​/users​/{id}​/role-mappings​/clients​/{client}​/available | https://datavaults-auth.euprojects.net/auth/personal-app/users​/{id}​/role-mappings​/clients​/{client}​/available | Retrieve roles that can be mapped to the user |
| **GET** | /{realm}/clients/{id}/roles | https://datavaults-auth.euprojects.net/auth/personal-app/clients/{id}/roles | Retrieve all roles of client |
| **GET** | /{realm}/roles | https://datavaults-auth.euprojects.net/auth/personal-app/roles | Retrieve all roles of realm |
| **GET** | /{realm}/users/{id}/role-mappings/realm | https://datavaults-auth.euprojects.net/auth/personal-app/users/{id}/role-mappings/realm | Get realm level role mapping |



Usefull link with Postman examples of the above Keycloak API calls and more: [Link](https://documenter.getpostman.com/view/7294517/SzmfZHnd#intro)
